Challenges of this Project.

- First: Non-technical challenges.

It took me 1 day and a half to read the reports of Statistical office and Ministry of Finance to check for the 
credibility and reliability of the dataset. Secondly, getting to read those lengthy reports was not a trivial task. 
It took me a while till I detected a pattern in terms of reporting of GDP in terms currency, and reports regarding deflatory GDP-s (in percentages). 

Once I got familiar with the numbers and concepts, I proceeded with uploading the csv file. 

Hours spent: In total 10 hours (1 day and a half).

- Second: Technical Challenges. 

The most challenging task of all was fixing and cleaning the data and checking for data credibility. 
Because the days spent with reading the reports regarding the GDPs was directly linked with the screening process of the numbers and reconfirming the credibility of the numbers in the csv file. 
Hence, cleaning the numbers that deviated from the published reports was my priority. Apart from that, a bulk of my time was spent values that may seem awkward, also; such as GDP expressed in percentages, etc. 

The second most devilish challenge was to convert the objects into int64. There was a little glitch right from the beggining by avoiding such error alerts. Such alerts were disengaged by implementing the following command pd.options.mode.chained_assignment = None, right from the beggining.
Specifically, the objects with decimal numbers (0.0) raised implicit errors. Apperently the code executed earlier prevented me from looking at the error. Gradually, I noticed the error and fixed it by completely erasing those numbers. The detailed explanations are mentioned up above.

The third most challenging task was to cast the Quarters from Objects, into pandas Datetime data type. 
Here the issue was quite interesting, because the Quarters were abbreviated with the T in Macedonian font - I believe. Which made it quite difficult to convert it. 

Indeed, it was surprising to see that some of the Quarters were converted while others were not. 
I fortunately assumed the issue lied within the data itself. I opened the file, and replaced all the (Py) with the letter T. The issue was fixed, instantly. 

The whole process was a bit tiring as I kept an eye on the credbility of the data. For every step in the way, I would re - visit the numbers from the published reports.

This was an interesting and very useful project to do.

Date: November, 2022
